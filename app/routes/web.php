<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Auth::routes();

Route::group(['middleware'=>['web']],function(){
    Route::get('/spice/{spice}',['as'=>'spice.view','uses'=>'PageController@viewSpice']);
    Route::get('/',['as'=>'main','uses'=>'PageController@main']);
    Route::get('/collection',['as'=>'spice.collection','uses'=>'PageController@collection']);
    Route::get('/company',['as'=>'about','uses'=>'PageController@about']);
    Route::get('/products',['as'=>'products','uses'=>'PageController@products']);
    Route::get('/partners',['as'=>'partners','uses'=>'PageController@partners']);

    Route::get('/cart',['as'=>'cart','uses'=>'CartController@cart']);
    Route::post('register-order',['as'=>'order.create','uses'=>'CartController@createOrder']);
    Route::get('order/{hash}',['as'=>'order.show','uses'=>'CartController@showOrder']);
    Route::post('/checkout',['as'=>'checkout','uses'=>'CartController@checkout']);
    Route::get('/collection/{spice}',['as'=>'collection.spice','uses'=>'CartController@collectionSpice']);
    Route::get('/checkout',function(){
        return redirect(route('cart'));
    });

    Route::post('feedback',['as'=>'feedback','uses'=>'PublicController@feedback']);
});


Route::group(['as'=>'admin.','prefix'=>'dashboard','namespace'=>'Admin'],function(){
    Route::get('main',function(){
        return view('dashboard.main');
    })->middleware('auth');
    Route::get('spice/new',['as'=>'spice.create','uses'=>'SpiceController@create']);
    Route::get('spice',['as'=>'spice.listing','uses'=>'SpiceController@listing']);
    Route::post('spice/new',['as'=>'spice.store','uses'=>'SpiceController@store']);
    Route::delete('spice/{spice}',['as'=>'spice.delete','uses'=>'SpiceController@delete']);
    Route::get('spice/{spice}',['as'=>'spice.edit','uses'=>'SpiceController@edit']);
    Route::put('spice/{spice}',['as'=>'spice.update','uses'=>'SpiceController@update']);

    Route::get('partner',['as'=>'partner.listing','uses'=>'PartnerController@listing']);
    Route::get('partner/new',['as'=>'partner.create','uses'=>'PartnerController@create']);
    Route::post('partner/new',['as'=>'partner.store','uses'=>'PartnerController@store']);
    Route::delete('partner/{partner}',['as'=>'partner.delete','uses'=>'PartnerController@delete']);

    Route::get('message',['as'=>'message.listing','uses'=>'MessageController@listing']);
    Route::get('message/{message}',['as'=>'message.show','uses'=>'MessageController@show']);
    Route::delete('message/{message}',['as'=>'message.delete','uses'=>'MessageController@delete']);

    Route::get('contact',['as'=>'contact.listing','uses'=>"ContactController@listing"]);
    Route::get('contact/edit',['as'=>'contact.edit','uses'=>"ContactController@edit"]);
    Route::post('contact',['as'=>'contact.update','uses'=>"ContactController@update"]);


    Route::get('container',['as'=>'container.listing','uses'=>'ContainerController@listing']);
    Route::get('container/new',['as'=>'container.create','uses'=>'ContainerController@create']);
    Route::post('container/new',['as'=>'container.store','uses'=>'ContainerController@store']);
    Route::delete('container/{container}',['as'=>'container.delete','uses'=>'ContainerController@delete']);
    Route::get('container/{container}',['as'=>'container.edit','uses'=>'ContainerController@edit']);
    Route::put('container/{container}',['as'=>'container.update','uses'=>'ContainerController@update']);


    Route::get('order/{status?}',['as'=>'order.listing','uses'=>'OrderController@listing']);
    
});

Route::group(['as'=>'api:','prefix'=>'api'],function(){
    Route::post('cart',['as'=>'add-to-cart','uses'=>'ApiController@addToCart']);
    Route::delete('cart/{product}',['as'=>'delete-from-cart','uses'=>'ApiController@deleteFromCart']);
    Route::delete('cart',['as'=>'empty-cart','uses'=>'ApiController@emptyCart']);
});