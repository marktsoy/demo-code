<?php

namespace App\Http\Controllers;

use App\Order;
use App\Transaction;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Validation\Rule;

/**
* @class PaycomController
* implementation of RPC Server according to docs
*/

class PaycomController extends Controller
{
    const METHODS = [
        'CheckPerformTransaction',
        'CheckTransaction',
        'CreateTransaction',
        'PerformTransaction',
        'CancelTransaction',
        'ChangePassword',
        'GetStatement'
    ];
    const ERROR_INTERNAL_SYSTEM = -32400;
    const ERROR_INSUFFICIENT_PRIVILEGE = -32504;
    const ERROR_INVALID_JSON_RPC_OBJECT = -32600;
    const ERROR_METHOD_NOT_FOUND = -32601;
    const ERROR_INVALID_AMOUNT = -31001;
    const ERROR_TRANSACTION_NOT_FOUND = -31003;
    const ERROR_INVALID_ACCOUNT = -31050;
    const ERROR_COULD_NOT_CANCEL = -31007;
    const ERROR_COULD_NOT_PERFORM = -31008;


    public function __construct(){
        //middleware sponsible for authorization | checking Ip | enctype | method
        //implementation in \App\Http\Middleware\PaycomGateMiddleware
        $this->middleware(['payment_gate']);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function router(Request $request)
    {
        $validator = Validator::make($request->only(['method','params','id']), [
            'method' => [
                'required',
                Rule::in(self::METHODS)
            ],
            'params' => 'required|array',
            'id'=> 'required|integer'
        ]);
        if ($validator->fails())
            return $this->error(self::ERROR_INVALID_JSON_RPC_OBJECT,'JSON объект не прошел валидацию',$validator->errors()->all());
        switch ($request->input('method')){
            case 'CheckPerformTransaction' : return $this->CheckPerformTransaction($request->input('params'));
            case 'CheckTransaction' : return $this->CheckTransaction($request->input('params'));
            case 'CreateTransaction' : return $this->CreateTransaction($request->input('params'));
            case 'PerformTransaction' : return $this->PerformTransaction($request->input('params'));
            case 'CancelTransaction' : return $this->CancelTransaction($request->input('params'));
            case 'ChangePassword' : return $this->ChangePassword($request->input('params'));
            case 'GetStatement' : return $this->GetStatement($request->input('params'));
        }
    }

    /**
     * @param $params @type array :amount,:account,:account.order_id
     * @param bool $fromRouter
     * @return bool|\Illuminate\Http\JsonResponse
     */
    private function CheckPerformTransaction($params,$fromRouter=true)
    {
        $validator = Validator::make($params,[
            'amount' => 'required|integer',
            'account' => 'required|array',
            'account.order_id' => 'required'
        ]);
        //check Request Params
        if ($validator->passes()) {
            $order = Order::whereId($params['account']['order_id'])
                ->first();
            //Order exists?
            if ($order) {
                //can order be paid?
                if ($order->status == Order::STATUS_PENDING) {
                    //check the amount
                    if ($order->amount == $params['amount']) {
                        if($fromRouter){
                            return $this->RPC(null,['allow'=>true]);
                        }
                        return true;
                    }
                    return $this->error(self::ERROR_INVALID_AMOUNT,'Сумма заказа не совпадает',['amount'=>$order->amount]);
                }
                return $this->error(-31050,'Условия состояния заказа не верны',['order_status'=>$order->status]);
            }
            return $this->error(-31051,'Заказ не найден',['order_id'=>$params['account']['order_id']]);
        }
        return $this->error(self::ERROR_INVALID_JSON_RPC_OBJECT,'Запрос не прошел валидацию');
    }


    /**
     * @param $params @type array :id, :method, :time, :account, :account.order_id
     * @return \Illuminate\Http\JsonResponse
     */
    private function CreateTransaction($params)
    {
        $validator = Validator::make($params, [
            'id'=>'required',
            'time' => 'required',
            'amount' => 'required|integer',
            'account' => 'required|array',
            'account.order_id' => 'required',
        ]);
        if ($validator->fails()) {
            return $this->error(self::ERROR_INVALID_JSON_RPC_OBJECT,'Запрос не прошел валидацию');
        }
        $transaction = Transaction::whereVendorId($params['id'])->first();
        //Transaction already exists in system
        if ($transaction) {
            if ($transaction->status == Transaction::STATE_CREATED) {
                if ($transaction->expired()) {
                    $transaction->status = -1;
                    $transaction->reason = Transaction::REASON_CANCELLED_BY_TIMEOUT;
                    $transaction->save();
                    return $this->error(self::ERROR_COULD_NOT_PERFORM,'Время ожидания транзакции истекло',['state'=>$transaction->status]);
                } else {
                    return $this->RPC(null,$transaction->toPaycom()->only(['create_time', 'transaction', 'state'])->all());
                }
            }
            return $this->error(self::ERROR_COULD_NOT_PERFORM,'Статус транзакции не верен',['state'=>$transaction->status]);
        }

        //Try to create transaction
        $allow = $this->CheckPerformTransaction($params,false);
        if($allow===true){
            $order = Order::whereId($params['account']['order_id'])->first();
            $order->status=Order::STATUS_WAITING;
            $order->save();
            $transaction = Transaction::create([
                'amount'=>$params['amount'],
                'order_id'=>$params['account']['order_id'],
                'vendor_id'=>$params['id'],
                'status'=>Transaction::STATE_CREATED,
                'perform_time'=>null,
                'time'=>round($params['time']/1000),
                'cancel_time'=>null,
            ]);
            return $this->RPC(null,$transaction->toPaycom()->only(['create_time','transaction','state'])->all());
        }
        return $allow;
    }


    /**
     * @param $params @type array :id
     * @return \Illuminate\Http\JsonResponse
     */
    private function CheckTransaction($params)
    {
        $validator = Validator::make($params,[
            'id'=>'required'
        ]);
        if($validator->fails())
            return $this->error(self::ERROR_INVALID_JSON_RPC_OBJECT,'Невалидный RPC объект',$validator->errors()->all());
        $transaction = Transaction::with('order')->whereVendorId($params['id'])->first();
        if(!$transaction)
            return $this->error(self::ERROR_TRANSACTION_NOT_FOUND,'Транзакция не была найдена',$validator->errors()->all());
        return $this->RPC(null,$transaction->toPaycom()
            ->only(['create_time','perform_time','cancel_time','transaction','state','reason'])->all());
    }



    /**
     * @param $params @type array :id
     * @return \Illuminate\Http\JsonResponse
     */
    private function PerformTransaction($params)
    {
        $validator = Validator::make($params,[
            'id'=>'required'
        ]);
        if($validator->fails())
            return $this->error(self::ERROR_INVALID_JSON_RPC_OBJECT,'Невалидный RPC объект',$validator->errors()->all());
        $transaction = Transaction::with('order')->whereVendorId($params['id'])->first();
        if(!$transaction)
            return $this->error(self::ERROR_TRANSACTION_NOT_FOUND,'Транзакция не была найдена',$validator->errors()->all());
        switch ($transaction->status){
            case Transaction::STATE_CREATED:
                if(!$transaction->expired()) {
                    $transaction->order->status = Order::STATUS_COMPLETED;
                    $transaction->order->save();
                    $transaction->perform_time = Carbon::now();
                    $transaction->status = Transaction::STATE_COMPLETED;
                    $transaction->save();
                    return $this->RPC(null,$transaction->toPaycom()
                        ->only(['transaction','perform_time','state'])->all());
                }else{
                    $transaction->status = Transaction::STATE_CANCELLED;
                    $transaction->reason = Transaction::REASON_CANCELLED_BY_TIMEOUT;
                    $transaction->save();
                    return $this->error(self::ERROR_COULD_NOT_PERFORM,'Транзакция отменена по таймауту',['time'=>$transaction->time->getTimestamp(),'state'=>$transaction->status]);
                }
            case Transaction::STATE_COMPLETED:
                return $this->RPC(null,$transaction->toPaycom()
                    ->only(['transaction','perform_time','state'])->all());
            default: return $this->error(self::ERROR_COULD_NOT_PERFORM,'Статус транзакции не позволяет совершить операцию',['state'=>$transaction->status]);
        }
    }



    /**
     * @param $params
     * @return \Illuminate\Http\JsonResponse
     */
    private function CancelTransaction($params)
    {
        //request params validation
        $validator = Validator::make($params,[
            'id'=>'required',
            'reason'=>'required|integer'
        ]);
        if($validator->fails())
            return $this->error(self::ERROR_INVALID_JSON_RPC_OBJECT,'Невалидный RPC объект',$validator->errors()->all());
        //try to find transaction
        $transaction = Transaction::with('order')->whereVendorId($params['id'])->first();
        if(!$transaction)
            return $this->error(self::ERROR_TRANSACTION_NOT_FOUND,'Транзакция не была найдена',$validator->errors()->all());
        switch ($transaction->status){
            case  Transaction::STATE_CREATED: {
                $transaction->status = Transaction::STATE_CANCELLED;
                $transaction->reason = $params['reason'];
                $transaction->cancel_time = Carbon::now();
                $transaction->save();
                return $this->RPC(null,$transaction->toPaycom()
                    ->only(['state','transaction','cancel_time'])->all());
            }
            case Transaction::STATE_COMPLETED:{
                if($transaction->order->status!=Order::STATUS_DELIVERED){
                    $transaction->order->status = Order::STATUS_CANCELLED;
                    $transaction->order->save();
                    $transaction->status = Transaction::STATE_CANCELLED_AFTER_COMPLETE;
                    $transaction->reason = $params['reason'];
                    $transaction->cancel_time = Carbon::now();
                    $transaction->save();
                    return $this->RPC(null,$transaction->toPaycom()
                        ->only(['state','transaction','cancel_time'])->all());
                }
                return $this->error(self::ERROR_COULD_NOT_CANCEL,'Заказ был доставлен клиенту',['order'=>$transaction->order->id]);
            }
            default: return $this->RPC(null,$transaction->toPaycom()
                ->only(['state','transaction','cancel_time'])->all());
        }
    }



    private function ChangePassword($params)
    {
        //TODO implement method
    }

    /**
     * @param $params @type array :from, :to
     * @return \Illuminate\Http\JsonResponse
     */
    private function GetStatement($params)
    {
        //request params validation
        $validator = Validator::make($params,[
            'from'=>'required|integer',
            'to'=>'required|integer',
        ]);
        if($validator->fails())
            return $this->error(self::ERROR_INVALID_JSON_RPC_OBJECT,'Невалидный RPC объект',$validator->errors()->all());
        $from = Carbon::createFromTimestamp($params['from']);
        $to = Carbon::createFromTimestamp($params['to']);
        $transactions = Transaction::orderBy('time')->get()
            ->filter(function($t) use ($from,$to){
                return $t->time->gte($from) && $t->time->lte($to);
            })
            ->map(function($t){
                return $t->toPaycom();
            })
            ->values()->toArray();
        return $this->RPC(null,['transactions'=>$transactions]);
    }

    /**
     * Wrap error RPC response
     * @param $code
     * @param string $message
     * @param null $data
     * @return \Illuminate\Http\JsonResponse
     */
    private function error($code, $message='Ошибка сервера', $data=null)
    {
        return $this->RPC([
            'code' => $code,
            'message' => [
                'ru' => $message,
            ],
            'data' => $data]);
    }

    /**
     * acts as wrapper for all responses from RPC Server
     * constructs RPC Response
     * @param $error
     * @param null $result
     * @return \Illuminate\Http\JsonResponse
     */
    private function RPC($error, $result=null)
    {
        return response()->json([
            'error' => $error,
            'result' => $result,
            'id' => request()->input('id'),
        ]);
    }
}
