<?php

namespace App\Http\Controllers;

use App\Contact;
use App\Order;
use App\Spice;
use Illuminate\Http\Request;
use Illuminate\Validation\Rule;

class CartController extends Controller
{
    /**
     * @param Spice $spice
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function collectionSpice(Spice $spice)
    {
        $spice->load('products');
        return view('ajax.product-picker', ['spice' => $spice]);
    }

    /**
     * @param Request $request
     * @return mixed
     */
    public function cart(Request $request)
    {
        $cart = $this->_getCart($request);
        return response(view('pages.cart', ['cart' => $cart]))
            ->cookie('cart', $cart->id, 30);
    }

    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function checkout(Request $request)
    {
        $cart = $this->_getCart($request);
        $sum = $cart->products->reduce(function ($c, $p) {
            return $c + $p->pivot->price;
        });
        $map_value = Contact::map()->first();
        $socials = Contact::socials();
        $general = Contact::general();
        return view('pages.checkout', ['price' => $sum,
            'cart' => $cart,
        ]);
    }


    /**
     * @param Request $request
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View | Redirect to PAYCOM_CHECKOUT
     */
    public function createOrder(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email',
            'phone' => 'required',
            'cart' => 'required|exists:orders,id',
            'payment_type' => [
                'required',
                Rule::in(['online', 'offline']),
            ],
        ]);
        $order = $this->_getCart($request);
        if (!$order || !$order->products->count())
            abort(404);
        $order->status = 1;
        $order->name = $request->name;
        $order->email = $request->email;
        $order->phone = $request->phone;
        $order->payment_type = $request->payment_type;
        $sum = $order->products()->get()->reduce(function ($c, $p) {
            return $c + $p->pivot->price;
        });
        $order->amount = 100*$sum;
        $order->save();
        if ($request->payment_type == 'online') {
            return redirect(env('PAYCOM_CHECKOUT').'/'.base64_encode(json_encode($this->_makePaycomUrl($order))));
        }
        return redirect(route('order.show', ['hash' => $order->hash]));
    }



    /**
    * Return page of the order
    * Link send to email & redirected after order registered
    * @param string $hash
    * @return  \Illuminate\Contracts\View\Factory|\Illuminate\View\View
    */
    public function showOrder($hash)
    {
        $order = Order::with(['products' => function ($q) {
            $q->with('spice');
        }])->whereHash($hash)->whereNotIn('status', [0])->first();
        if (!$order)
            abort(404);
        else
            return view('pages.order', ['order' => $order]);
    }

    /**
    * retrieve cart from request|cookie
    * @param Request $request
    * @return App\Cart
    **/
    private function _getCart(Request $request)
    {
        $cart = null;
        if ($request->has('cart') && $request->cart)
            $cart = Order::with(['products' => function ($q) {
                    $q->with('spice');
                }
            ])->whereStatus(0)
                ->whereId($request->cart)
                ->first();
        elseif ($cart_id = $request->cookie('cart')) {
            $cart = Order::with([
                'products' => function ($q) {
                    $q->with('spice');
                }
            ])
                ->whereStatus(0)
                ->whereId($cart_id)
                ->first();
        }
        //nothing found
        if (!$cart) {
            $cart = Order::create(['status' => 0]);
        }
        return $cart;
    }

    /**
    * @param \App\Order $order
    * @return array
    */
    private function _makePaycomUrl($order)
    {
        //m	ID или алиас поставщика
        //ac	объект Account
        //a	сумма платежа
        //l	язык пользователя. Доступные значения: ru, uz, en
        //c	URL адрес для возврата после оплаты или отмены платежа
        //ct	таймаут возврата после успешного платежа в милисекундах
        $sum = $order->products()->get()->reduce(function ($c, $p) {
            return $c + $p->pivot->price;
        });
        return [
            'm' => env("PAYCOM_MERCHANT_ID"),
            'ac' => [
                'order_id' => $order->id,
            ],
            'a' => $order->amount,
            'l' => 'ru',
            'c' => route('order.show', ['hash' => $order->hash])
        ];
    }
}
