<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    /** Order is not submitted. Seen as cart */
    const STATUS_CART = 0;

    /** Pending status, changes when redirected from  Paycom or administrator approves it*/
    const STATUS_PENDING = 1;

    /** Pay in progress, order must not be changed. */
    const STATUS_WAITING = 2;

    /** Order completed and not available for sell.  Transaction Created*/
    const STATUS_COMPLETED = 3;

    /** Order is cancelled. */
    const STATUS_CANCELLED = 4;

    const STATUS_DELIVERED = 5;

    protected $fillable = ['status','name','email','phone','payment_type','hash'];
    protected $primaryKey = 'id';
    public $incrementing = false;

    /**
    * HasMany Relation
    */
    public function products(){
        return $this->belongsToMany('App\Product','order_products','order_id','product_id')->withPivot(['quantity','price']);
    }

    /**
    * Has One Transaction relation
    */
    public function transaction(){
        return $this->hasOne('App\Transaction','order_id');
    }

    /**
    * helper method
    * TODO: return status from locale dictionary
    */
    public function getStrStatus(){
        switch ($this->status){
            case 1: return 'Заказ принят';
            case 2: return 'Заказ ожидает оплаты';
            case 3: return 'Заказ осуществлен';
            case 4: return 'Заказ Отменен';
            default: return 'Корзина';
        }
    }
    //
}
