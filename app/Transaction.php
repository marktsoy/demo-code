<?php

namespace App;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    const TIMEOUT = 43200000;

    const STATE_CREATED = 1;
    const STATE_COMPLETED = 2;
    const STATE_CANCELLED = -1;
    const STATE_CANCELLED_AFTER_COMPLETE = -2;

    const REASON_RECEIVERS_NOT_FOUND = 1;
    const REASON_PROCESSING_EXECUTION_FAILED = 2;
    const REASON_EXECUTION_FAILED = 3;
    const REASON_CANCELLED_BY_TIMEOUT = 4;
    const REASON_FUND_RETURNED = 5;
    const REASON_UNKNOWN = 10;


    protected $fillable = [
        'amount',
        'order_id',
        'vendor_id',
        'status',
        'perform_time',
        'time',
        'cancel_time'
    ];
    public $incrementing = false;
    public $dates = [
        'created_at',
        'updated_at',
        'perform_time',
        'cancel_time',
        'time'
    ];

    /**
     * 1 To 1 relation with order
     * @return \Illuminate\Database\Eloquent\Relations\BelongsTo
     */
    public function order(){
        return $this->belongsTo('App\Order','order_id');
    }

    /**
     * transforms model into Paycom  Valid object
     * use collection methods like ::only() to retieve fragments
     * @method toPaycom
     * @return Laravel Collection
     */
    public function toPaycom(){
        return collect([
            'id'=>$this->vendor_id,
            'time'=>$this->time->getTimestamp()*1000,
            'amount'=>$this->amount,
            'account'=>[
                'order_id'=>$this->order_id
            ],
            'create_time'=>$this->created_at->getTimestamp()*1000,
            'perform_time'=>$this->perform_time ? $this->perform_time->getTimestamp()*1000 :null,
            'cancel_time'=>$this->cancel_time ? $this->cancel_time->getTimestamp()*1000 :null,
            'transaction'=>$this->id,
            'state'=>$this->status,
            'reason'=>$this->reason
        ]);
    }

    /**
     * @method expired
     * @return boolean
     */
    public function expired(){
        $expires_at = $this->time->copy();
        $expires_at->addHours(12);
        return  Carbon::now()->gte($expires_at);

    }

    //
}
