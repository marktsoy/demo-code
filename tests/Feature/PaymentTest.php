<?php

namespace Tests\Feature;

use App\Order;
use App\Transaction;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;

class PaymentTest extends TestCase
{
    use DatabaseTransactions;

    /**
     * RPC CheckPerformTransaction test.
     *
     * @return void
     */
    public function testCheckPerformTransaction()
    {
        $order = Order::create(['status' => Order::STATUS_PENDING, 'name' => 'Some tester', 'email' => 'make-test@env.net', 'phone' => '+1312323', 'payment_type' => 'online', 'hash' => md5(time())]);
        $order->amount = 12345 * 100;
        $order->save();
        $request = [
            'id' => 1,
            'method' => 'CheckPerformTransaction',
            'params' => [
                'account' => [
                    'order_id' => $order->id
                ],
                'amount' => 12345 * 100,
            ]
        ];
        $this->json('POST', 'api/paycom', $request)
            ->assertExactJson([
                'error' => null,
                'result' => [
                    'allow' => true,
                ],
                'id' => 1
            ]);
        $this->json('POST', 'api/paycom', ['id' => 1,
            'method' => 'CheckPerformTransaction',
            'params' => [
                'account' => [
                    'order_id' => "sdasdasd"
                ],
                'amount' => 12345 * 100,
            ]])
            ->assertJsonFragment([
                'error' => [
                    'code' => -31051,
                    'data' => [
                        'order_id' => "sdasdasd"
                    ],
                    'message' => [
                        'ru' => 'Заказ не найден'
                    ]
                ],
                'result' => null,
                'id' => 1
            ]);
    }

    /**
    * Test RPC CreateTransaction with valid data
    */
    public function testCreateTransactionValid()
    {
        $order = Order::create([
            'status' => Order::STATUS_PENDING,
            'name' => 'create Transaction',
            'email' => 'test@env.dev',
            'phone' => '123434',
            'payment_type' => 'online',
            'amount' => 40000,
        ]);
        $order->amount = 400 * 100;
        $order->save();
        $rpcReqSuccess = [
            'method' => 'CreateTransaction',
            'params' => [
                'id' => 123,
                'time' => time() * 1000,
                'account' => [
                    'order_id' => $order->id
                ],
                'amount' => 40000,
            ],
            'id' => 1232
        ];
        $this->json('post', 'api/paycom', $rpcReqSuccess)
//            ->assertSee('asd')
            ->assertStatus(200)
            ->assertJson([
                'result' => [
                    'state' => Transaction::STATE_CREATED,
                ],
                'id' => 1232
            ]);

        $this->json('post', 'api/paycom', $rpcReqSuccess)
            ->assertStatus(200)
            ->assertJson([
                'result' => [
                    'state' => Transaction::STATE_CREATED,
                ],
                'id' => 1232
            ]);



    }

    /**
    * Test RPC CreateTransaction with invalid data
    */
    public function testCreateTransactionInvalid()
    {
        $order = Order::create([
            'status' => Order::STATUS_PENDING,
            'name' => 'create Transaction Invalid',
            'email' => 'invalid@create.transaction',
            'phone' => '2123123',
            'payment_type' => 'online',
        ]);
        $order->amount = 2000 * 100;
        $order->save();

        //test behaviour on wrong order_id sent
        $rpcReqError = [
            'method' => 'CreateTransaction',
            'params' => [
                'id' => 420,
                'time' => time() * 1000,
                'account' => [
                    'order_id' => 'something-not-uuid'
                ],
                'amount' => $order->amount,
            ],
            'id' => 12345
        ];
        $this->json('post', 'api/paycom', $rpcReqError)
            ->assertStatus(200)
            ->assertJson([
                'error' => [
                    'code' => -31051,
                    'message' => [
                        'ru' => 'Заказ не найден',
                    ],
                    'data' => [
                        'order_id' => 'something-not-uuid'
                    ]
                ],
                'id' => 12345
            ]);


        //Test Behaviour on Wrong amount send
        $rpcReqWrongAmount = [
            'method' => 'CreateTransaction',
            'params' => [
                'id' => 2,
                'time' => time() * 1000,
                'account' => [
                    'order_id' => $order->id
                ],
                'amount' => $order->amount + 1,
            ],
            'id' => 12345
        ];
        $this->json('post', 'api/paycom', $rpcReqWrongAmount)
            ->assertStatus(200)
            ->assertJson([
                'error' => [
                    'code' => -31001,
                    'message' => [
                        'ru' => 'Сумма заказа не совпадает',
                    ],
                    'data' => [
                        'amount' => $order->amount
                    ]
                ],
                'id' => 12345
            ]);
    }

    /**
    * Test RPC PerformTransaction with valid data
    */
    public function testPerformTransactionValid(){

        $order = Order::create([
            'status' => Order::STATUS_PENDING,
            'name' => 'create Transaction',
            'email' => 'test@env.dev',
            'phone' => '123434',
            'payment_type' => 'online',
            'amount' => 40000,
        ]);
        $order->amount = 400 * 100;
        $order->save();
        $rpcReqSuccess = [
            'method' => 'CreateTransaction',
            'params' => [
                'id' => 123,
                'time' => time() * 1000,
                'account' => [
                    'order_id' => $order->id
                ],
                'amount' => 40000,
            ],
            'id' => 1232
        ];

        $this->json('post', 'api/paycom', $rpcReqSuccess)
//            ->assertSee('asd')
            ->assertStatus(200)
            ->assertJson([
                'result' => [
                    'state' => Transaction::STATE_CREATED,
                ],
                'id' => 1232
            ]);

        $transaction = Transaction::whereVendorId(123)->first();
        $valid = [
            'method'=>'PerformTransaction',
            'params'=>[
                'id'=>123
            ],
            'id'=>123456
        ];
        $this->json('post','api/paycom',$valid)
            ->assertStatus(200)
            ->assertJson([
                'id'=>123456,
                'result'=>[
                    'transaction'=>$transaction->id,
                    'state'=>Transaction::STATE_COMPLETED
                ]
            ]);
    }

    //TODO: Perform with invalid| CancelTransaction| GetStatement | Check Transaction | ChangePassword

}
